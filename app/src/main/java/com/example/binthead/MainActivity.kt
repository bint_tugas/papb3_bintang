package com.example.binthead

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView

class MainActivity : AppCompatActivity() {

// Deklarasi Variabel
private lateinit var rambut: ImageView
private lateinit var alis: ImageView
private lateinit var jenggot: ImageView
private lateinit var kumis: ImageView
private lateinit var cekBoxRambut: CheckBox
private lateinit var cekBoxAlis: CheckBox
private lateinit var cekBoxJenggot: CheckBox
private lateinit var cekBoxKumis: CheckBox

        override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Inisialisasi Variabel
        rambut = findViewById(R.id.Rambut)
        alis = findViewById(R.id.Alis)
        kumis = findViewById(R.id.Kumis)
        jenggot = findViewById(R.id.Jenggot)
        cekBoxRambut = findViewById(R.id.CekBoxRambut)
        cekBoxAlis = findViewById(R.id.CekBoxAlis)
        cekBoxJenggot = findViewById(R.id.CekBoxjanggut)
        cekBoxKumis = findViewById(R.id.CekBoxKumis) // inisialisasi CekBoxKumis

        // Metode Cek Rambut
        cekBoxRambut.setOnClickListener {
        rambut.visibility = if (cekBoxRambut.isChecked) View.VISIBLE else View.GONE
        }

        // Metode Cek Alis
        cekBoxAlis.setOnClickListener {
        alis.visibility = if (cekBoxAlis.isChecked) View.VISIBLE else View.GONE
        }

        // Metode Cek Kumis
        cekBoxKumis.setOnClickListener {
        kumis.visibility = if (cekBoxKumis.isChecked) View.VISIBLE else View.GONE
        }

        // Metode Cek Jenggot
        cekBoxJenggot.setOnClickListener {
        jenggot.visibility = if (cekBoxJenggot.isChecked) View.VISIBLE else View.GONE
        }
        }
        }
